using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using System;
using System.Text.RegularExpressions;

namespace LatexToCSharpWebApi
{
    public class ExpressionDecoder
    {
        public Dictionary<string, string> syntaxTemplates;
        public List<char> ignoreCharacters;

        public string DecodeBody(string expression)
        {

            string header = expression.Split('=')[0];
            string body = expression.Split('=')[1];
            body = CleanBody(body);
            
            string[] paramsList = header.Split('(')[1].Split(',');
            for (int i = 0; i < paramsList.Length; i++)
            {
                if (paramsList[i].Contains(')')) paramsList[i] = paramsList[i].Remove(paramsList[i].IndexOf(')'), 1);
                if (paramsList[i].Contains('(')) paramsList[i] = paramsList[i].Remove(paramsList[i].IndexOf('('), 1);
            }
            
            string codeReturnValue = mainDecoding(body, paramsList);
            return "\treturn " + codeReturnValue + ";";
        }
        public string mainDecoding(string body, string[] paramsList)
        {
            if (body.Contains(")("))
                body = body.Replace(")(", ")*(");
            string codeReturnValue = "";
            List<string> tokenList = new List<string>();
            ignoreCharacters = new List<char>();
            ignoreCharacters.Add('+');
            ignoreCharacters.Add('-');
            ignoreCharacters.Add('=');
            ignoreCharacters.Add('*');
            ignoreCharacters.Add('/');
            ignoreCharacters.Add('\\');
            ignoreCharacters.Add('^');
            ignoreCharacters.Add('}');
            ignoreCharacters.Add('{');
            ignoreCharacters.Add(')');
            ignoreCharacters.Add('(');
            ignoreCharacters.Add(']');
            ignoreCharacters.Add('[');
            syntaxTemplates = new Dictionary<string, string>();
            syntaxTemplates.Add("^", "System.Math.Pow($p1,$p2)");
            syntaxTemplates.Add("\\\\log_", "System.Math.Log($p2,$p1)");
            syntaxTemplates.Add("\\\\pi", "System.Math.PI");
            syntaxTemplates.Add("\\\\frac", "$p1/$p2");
            syntaxTemplates.Add("\\\\sqrt", "System.Math.Pow($p2,1/$p1)");
            string syntaxRecognision = "";
            bool parameterAdded = false;

            for (int i = 0; i < body.Length; i++)
            {
                syntaxRecognision += body[i];

                if (paramsList.Contains(syntaxRecognision))
                {
                    if (parameterAdded)
                    {
                        tokenList.Add("*");
                    }
                    tokenList.Add(syntaxRecognision);
                    parameterAdded = true;
                    syntaxRecognision = "";
                }

                else if (isNumber(body[i]))
                {
                    if (body.Length > i + 1)
                        if (isIgnoredCharacter(body[i + 1]) && body[i + 1] != '.')
                        {
                            tokenList.Add(syntaxRecognision);
                            syntaxRecognision = "";
                        }

                        else if (!isIgnoredCharacter(body[i + 1]) && !isNumber(body[i + 1]) && body[i + 1] != '.')
                        {
                            tokenList.Add(syntaxRecognision);
                            syntaxRecognision = "";
                            tokenList.Add("*");
                        }
                }

                else if (isIgnoredCharacter(body[i]))
                {
                    if (body[i] == '\\')
                    {

                        for (int j = i + 1; j < body.Length; j++)
                        {
                            syntaxRecognision += body[j];
                            if (syntaxTemplates.ContainsKey(syntaxRecognision))
                            {
                                i = j;
                                break;
                            }
                        }

                    }

                    else if (body[i] == '(' || body[i] == '{' || body[i] == '[')
                    {
                        syntaxRecognision = "";
                        string tmpToken = "";
                        int bracetLevel = 0;

                        for (int j = i; j < body.Length; j++)
                        {
                            tmpToken += body[j];

                            if (body[j] == ')' || body[j] == '}' || body[j] == ']')
                            {
                                bracetLevel--;

                            }

                            if (body[j] == '{' || body[j] == '(' || body[j] == '[')
                            {
                                bracetLevel++;
                            }

                            if (bracetLevel == 0)
                            {
                                tmpToken = mainDecoding(tmpToken.Substring(1, tmpToken.Length - 2), paramsList);

                                if (tmpToken.Length > 1)
                                    tmpToken = "(" + tmpToken + ")";
                                i = j;
                                break;
                            }
                        }
                        tokenList.Add(tmpToken);
                    }

                    if (syntaxRecognision.Length > 0)
                    {
                        tokenList.Add(syntaxRecognision);
                        parameterAdded = false;

                        if (i + 1 < body.Length)
                        {
                            if (syntaxRecognision == "\\\\sqrt" && body[i + 1] != '[') tokenList.Add("2");
                            if (syntaxRecognision == "\\\\log_" && body[i + 1] != '{')
                            {
                                tokenList.Add(body[i + 1] + "");
                                i = i + 1;
                            }

                            if (syntaxRecognision == "\\\\pi"
                            && (isNumber(body[i + 1]) || (!isIgnoredCharacter(body[i + 1])
                            && !isNumber(body[i + 1])) || body[i + 1] == '\\')
                            && body[i + 1] != '*')
                                tokenList.Add("*");
                        }

                        syntaxRecognision = "";
                    }
                }

            }

            if (syntaxRecognision.Length > 0)
            {
                tokenList.Add(syntaxRecognision);
            }

            printTokenList(tokenList);
            for (int i = 0; i <= tokenList.Count() - 1; i++)
            {
                if (syntaxTemplates.ContainsKey(tokenList[i]))
                {
                    if (tokenList[i] == "^")
                    {
                        codeReturnValue = codeReturnValue.Remove(codeReturnValue.LastIndexOf(tokenList[i - 1]), tokenList[i - 1].Length);
                        if (tokenList[i + 1] == "-")
                        {
                            codeReturnValue += syntaxTemplates[tokenList[i]].Replace("$p1", tokenList[i - 1]).Replace("$p2", tokenList[i + 1] + tokenList[i + 2]);
                            i += 2;
                        }

                        else if (syntaxTemplates.ContainsKey(tokenList[i + 1]))
                        {
                            if (tokenList[i + 1] == "\\\\frac" || tokenList[i] == "\\\\sqrt" || tokenList[i] == "\\\\log_")
                            {
                                if (i + 3 < tokenList.Count)
                                {
                                    tokenList[i + 1] = syntaxTemplates[tokenList[i + 1]].Replace("$p1", tokenList[i + 2]).Replace("$p2", tokenList[i + 3]);
                                    tokenList.RemoveAt(i + 2);
                                    tokenList.RemoveAt(i + 2);
                                }
                                codeReturnValue += syntaxTemplates[tokenList[i]].Replace("$p1", tokenList[i - 1]).Replace("$p2", tokenList[i + 1]);
                                i = i + 1;
                                printTokenList(tokenList);
                            }
                        }

                        else
                        {
                            codeReturnValue += syntaxTemplates[tokenList[i]].Replace("$p1", tokenList[i - 1]).Replace("$p2", tokenList[i + 1]);
                            i += 1;
                        }

                    }

                    else if (tokenList[i] == "\\\\frac" || tokenList[i] == "\\\\sqrt" || tokenList[i] == "\\\\log_")
                    {

                        if (i + 2 < tokenList.Count)
                        {
                            codeReturnValue += syntaxTemplates[tokenList[i]].Replace("$p1", tokenList[i + 1]).Replace("$p2", tokenList[i + 2]);
                            tokenList[i + 2] = syntaxTemplates[tokenList[i]].Replace("$p1", tokenList[i + 1]).Replace("$p2", tokenList[i + 2]);
                        }
                        if (i < tokenList.Count)
                            i += 2;
                    }

                    else if (tokenList[i] == "\\\\pi")
                    {
                        if (isNumber(tokenList[i - 1][tokenList[i - 1].Length - 1]))
                            codeReturnValue += '*';
                        codeReturnValue += syntaxTemplates[tokenList[i]];
                    }

                }

                else codeReturnValue += tokenList[i];
            }

            return codeReturnValue;
        }
        public string CleanBody(string expression)
        {
            string tmp = expression;

            while (tmp.IndexOf(' ') > -1)
            {
                tmp = tmp.Remove(tmp.IndexOf(' '), 1);
            }

            while (tmp.IndexOf(',') > -1)
            {
                tmp = tmp.Replace(',', '.');
            }

            return tmp;
        }

        public void printTokenList(List<string> arg)
        {
            arg.ForEach((singleParam)=>{Console.WriteLine(singleParam);});
        }
        private bool isNumber(char arg)
        {
            return (arg <= '9' && arg >= '0');
        }
        private bool isIgnoredCharacter(char arg)
        {
            return ignoreCharacters.Contains(arg);
        }
        public string GenerateParameters(string expression)
        {
            var methodParams = "";
            List<MethodParameters> methodParamsList = new List<MethodParameters>();
            expression = expression.Substring(expression.IndexOf("(") + 1, expression.IndexOf(")") - 1);

            var parameterFromExpression = GetWords(expression);

            foreach (var item in parameterFromExpression.Distinct())
            {
                methodParamsList.Add(new MethodParameters("double", item.ToString()));
            }

            methodParamsList.ForEach(element =>
            {
                methodParams = methodParams + ' ' + element.type + ' ' + element.name + ',';
            });
            methodParams = methodParams.Substring(1, methodParams.Length - 2);

            return methodParams;
        }

        static string[] GetWords(string input)
        {
            MatchCollection matches = Regex.Matches(input, @"\b[a-zA-Z']*\b");

            var words = from m in matches.Cast<Match>()
                        where !string.IsNullOrEmpty(m.Value)
                        select TrimSuffix(m.Value);

            return words.ToArray();
        }

        static string TrimSuffix(string word)
        {
            int apostropheLocation = word.IndexOf('\'');
            if (apostropheLocation != -1)
            {
                word = word.Substring(0, apostropheLocation);
            }

            return word;
        }
    }
}