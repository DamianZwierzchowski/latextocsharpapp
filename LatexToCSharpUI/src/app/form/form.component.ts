import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpParams } from '@angular/common/http';
import { HttpResponse } from '@angular/common/http';
import { FormControl, Validators, PatternValidator } from '@angular/forms';
import { ACCESSMODIFIERS } from '../shared/consts';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})

export class FormComponent implements OnInit {
  constructor(private http: HttpClient) { }
  expression: string = "y(x)=\\frac{1}{x}";
  methodName = "exampleMethod";
  result = "";
  isValid = false;

  accessModifiers = ACCESSMODIFIERS;
  selectedAccessModifier = 'public';
  selectedType = 'int';

  methodParameters: string = "";

  expressionControl = new FormControl('', [
    Validators.required,
    Validators.maxLength(60),
    Validators.pattern(/^[^0-9][\w+\S]*(?:(\([a-zA-Z0-9,\S]+\)))\s*\=\s*((?:(\([\w \{ \( \[ \] \) \} \+ \- \* \^ \\ \/ \s]+\)))+|(?:(\{[\w \{ \( \[ \] \) \} \+ \- \* \^ \\ \/ \s]+\}))+|(?:(\[[\w \{ \( \[ \] \) \} \+ \- \* \^ \\ \/ \s]+\]))+|[\w \+ \- \* \^ \\ \/ \s]+|)+$/)
  ]);

  methodNameControl = new FormControl('', [
    Validators.required,
    Validators.maxLength(30),
    Validators.pattern(/^[^0-9][\w+\S]+$/)
  ]);

  isValidForm() {
    this.isValid = this.expressionControl.valid && this.methodNameControl.valid ? true : false;
    return this.isValid;
  }

  generateMethod() {
    this.result = '';
    this.http.post('http://localhost:5000/api/Values/GetMethodData/', { expression: this.expression }).subscribe(receivedData => {
      this.result = this.selectedAccessModifier + ' ' + 'double' + ' ' + this.methodName.toString() + '(' +
        receivedData[0] + '){' + '\n' + receivedData[1] + '\n' + '}';
    });
  }

  ngOnInit() {
  }
}
